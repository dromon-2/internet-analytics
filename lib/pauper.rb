# frozen_string_literal: true

require_relative "pauper/version"

module Pauper
  class Error < StandardError; end
  # Your code goes here...
end
