# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name = 'Pauper'
  spec.version       = '1.0'
  spec.authors       = ['Rytsaryer']
  spec.email         = ['rytsaryer@yandex.ru']

  spec.summary       = 'Pauper Bot'
  spec.description   = 'Program for automatic targeting'
  spec.homepage      = 'https://gitlab.com/dromon-2/internet-analytics'
  spec.license       = 'MIT'
  spec.required_ruby_version = '>= 3.0.0'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
